def format_position(position):
    rank, file = position[0], position[1]
    return rank, file

def rook(position):
    position = position.upper()
    rank, file = format_position(position)
    verticals = {rank + str(i) for i in range(1, 9)}
    horizontals = {chr(j) + file for j in range(65, 73)}

    return sorted(list(verticals ^ horizontals))

print(rook("h8"))







