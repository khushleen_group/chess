import knight
import rook
import bishop
import queen

def can_attack(piece1: str, pos1: str, piece2: str, pos2: str):
    
    def return_piece_fxn(piece: str, pos: str):
        if piece == 'K':
            return knight.knight(pos)
        elif piece == 'R':
            return rook.rook(pos)
        elif piece == 'B':
            return bishop.bishop(pos)
        else:
            return queen.queen(pos)
        
    piece1_attacks, piece2_attacks = pos2 in return_piece_fxn(piece1, pos1), pos1 in return_piece_fxn(piece2, pos2)

    if piece1_attacks and piece2_attacks:
        print("Both pieces can attack each other")
    elif piece1_attacks and not piece2_attacks:
        print(f'{piece1} attacks {piece2}')
    elif not piece1_attacks and piece2_attacks:
        print(f'{piece2} attacks {piece1}')
    else:
        print("Neither pieces can attack each other")
    
