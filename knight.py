from itertools import product

def knight(position):

    possibilities = []
    
    rank, file = position[0], position[1]
    one_ranks = [chr(ord(rank) + i) for i in {1,-1} if 'A' <= chr(ord(rank.upper()) + i) <= 'H' ]
    one_files = [chr(ord(file) + j) for j in {1,-1} if '0' < chr(ord(file) + j) < '9']
    two_ranks = [chr(ord(rank) + k) for k in {2,-2} if 'A' <= chr(ord(rank.upper()) + k) <= 'H']
    two_files = [chr(ord(file) + l) for l in {2,-2} if '0' < chr(ord(file) + l) < '9']
    return [element[0] + element[1] for element in list(product(one_ranks, two_files)) + list(product(two_ranks, one_files))]
