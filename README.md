# Chess pieces movement
Write functions which compute the possible moves of the chess piece when the current position is given
Input is given in string format. Example "A8", "G5"

We used the following functions:

1. rook(): returns the possible moves for rook
2. bishop(): returns the possible moves for bishop
3. queen(): returns the possible moves for queen
4. knight(): return the possible moves for knight


Using these to check if 2 pieces can attack each other
Function used: can\_attack()

