from bishopmoves import bishop
from rook import rook
def queen(position: str) -> list[str]:
    return bishop(position) + rook(position)

print(queen('A8'))

from math import factorial
print(len(str(factorial(25))))