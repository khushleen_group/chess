RANKS = 'ABCDEFGH'
FILES = '12345678'

def bishop(position):

    possibilities = []

    a,n = position[0], position[1]
    rank1, rank2 = RANKS.split(a)
    file1, file2 = FILES.split(n)

    part1 = list(zip(reversed(list(rank1)), reversed(list(file1)))) 
    part2 = list(zip(list(rank2),list(file2))) 
    part3 = list(zip(reversed(list(rank1)), list(file2))) 
    part4 = list(zip(list(rank2), reversed(list(file1))))

    positions = part1 + part2 + part3 + part4
                                                    
    return [element[0] + element[1] for element in positions]

